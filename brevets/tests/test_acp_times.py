from acp_times import open_time, close_time

import nose
import arrow
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)
log = logging.getLogger(__name__)

#Test case assertion answers from https://rusa.org/pages/acp-brevet-control-times-calculator

empty_date = "2017-01-01 00:00"

def test_1():

    correct_output = "2017-01-02T05:09:00+00:00"
    start_time = arrow.get(empty_date).isoformat()
    my_output = open_time(890, 1000, start_time)
    assert str(my_output) == correct_output

def test_2():

    correct_output = "2017-01-01T17:08:00+00:00"
    start_time = arrow.get(empty_date).isoformat()
    my_output = open_time(550, 600, start_time)
    assert str(my_output) == correct_output

def test_closing():

    correct_output = "2017-01-03T17:23:00+00:00"
    start_time = arrow.get(empty_date).isoformat()
    my_output = close_time(890, 1000, start_time)
    assert str(my_output) == correct_output

def test_twenty():

    correct_output = "2017-01-01T05:53:00+00:00"
    start_time = arrow.get(empty_date).isoformat()
    my_output = open_time(250, 200, start_time)
    assert str(my_output) == correct_output

def test_zero():

    correct_output = "2017-01-01T00:00:00+00:00"
    start_time = arrow.get(empty_date).isoformat()
    my_output = close_time(0, 200, start_time)
    assert str(my_output) == correct_output
