"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from dateutil import tz

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    speed = [(200,15,34),
             (400,15,32),
             (600,15,30),
             (1000,11.428,28),
             (1300,13.333,26)]

    i = 0
    length = 0
    time = 0
    if(control_dist_km<brevet_dist_km):
        while((length + control_dist_km) > speed[i][0]):
            time += (speed[i][0] - length)/speed[i][2]
            control_dist_km -= (speed[i][0] - length)
            length += (speed[i][0] - length)
            i += 1
        time += control_dist_km / speed[i][2]
    else:
        while((length + brevet_dist_km) > speed[i][0]):
            time += (speed[i][0] - length)/speed[i][2]
            brevet_dist_km -= (speed[i][0] - length)
            length += (speed[i][0] - length)
            i += 1
        time += brevet_dist_km / speed[i][2]

    minute = time % 1
    hour = time - minute

    a = arrow.get(brevet_start_time).shift(days=0,hours=hour,minutes=round(minute * 60))
    return a.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    speed = [(200,15,34),
              (400,15,32),
              (600,15,30),
              (1000,11.428,28),
              (1300,13.333,26)]

    i = 0
    distance = 0
    time = 0
    if (control_dist_km >= brevet_dist_km):
        max = {200:1330, 300:2000, 400:2700, 600:4000, 1000:7500}
        a = arrow.get(brevet_start_time).shift(days=0,hours=(-4+(max[brevet_dist_km]//100)),minutes =  max[brevet_dist_km]%100)
    else:
        while((distance + control_dist_km) > speed[i][0]):
                time += (speed[i][0] - distance)/speed[i][1]
                control_dist_km -=  (speed[i][0] - distance)
                distance += (speed[i][0] - distance)
                i += 1

        time += control_dist_km / speed[i][1]
        minute = time % 1
        hour = time - minute

    a = arrow.get(brevet_start_time).shift(days=0,hours=hour,minutes=round(minute * 60))
    return a.isoformat()

