# Author
## Kyra Novitzky

## Contact Address
knovitzk@uoregon.edu

## Description

Essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.
Each time a specified distance is filled in on the web browser, Ajax will respond by filling in the opening and closing times.

# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.